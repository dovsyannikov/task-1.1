package com.company;

import java.util.Objects;

public class BookShelf {
    Books[] book = new Generator().generateBook();

    public void findBookWithSpecificAuthorAndHaveCoAuthors(String specificAuthor){
        for (int i = 0; i < book.length; i++) {
            int countOfAuthorsinCurrentBook = book[i].author.length;
            int currentBookNumber = i;
            for (int j = 0; j < countOfAuthorsinCurrentBook; j++) {
                if (book[currentBookNumber].author[j].toLowerCase().equals(specificAuthor.toLowerCase()) && countOfAuthorsinCurrentBook > 1) {
                    System.out.println("Название книги: " + book[currentBookNumber].nameOfTheBook + ", Издательство: " +
                            book[currentBookNumber].publisherName + ", Год Издания: " + book[currentBookNumber].createdDate + ", Authors: " +
                            returnAllTheAuthors(book[currentBookNumber]).toString().toUpperCase());
                }
            }

        }
    }
    public void findTheBooksWithThreeOrMoreAuthors(){
        for (int i = 0; i < book.length; i++) {
            if (book[i].author.length >= 3){
                System.out.println("Название книги: " + book[i].nameOfTheBook + ", Издательство: " +
                        book[i].publisherName + ", Год Издания: " + book[i].createdDate + ", Authors: " +
                        returnAllTheAuthors(book[i]).toString().toUpperCase());
            }
        }
    }
    public StringBuilder returnAllTheAuthors (Books book){
        int countOfAuthors = book.author.length;
        StringBuilder builder = new StringBuilder();
        for (int j = 0; j < countOfAuthors; j++) {

            builder.append(book.author[j]);
            if (j != countOfAuthors - 1){
                builder.append(", ");
            }
        }
        return builder;
    }
}
