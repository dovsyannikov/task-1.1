package com.company;

public class Books {
    public String nameOfTheBook;
    public int createdDate;
    public String publisherName;
    String[] author;

    public Books(String publisherName, int createdDate, String nameOfTheBook, String[] author){
        this.createdDate = createdDate;
        this.nameOfTheBook = nameOfTheBook;
        this.author = author;
        this.publisherName = publisherName;
    }
}
