package com.company;

public class Generator {
    public Books[] generateBook() {
        Books[] book = new Books[8];

        book[0] = new Books("Random Publisher all the same", 1719, "Robinson Crusoe", new String[] {"Daniel Defou"});
        book[1] = new Books("Random Publisher all the same",1869, "War and Peace", new String[] {"Leo Tolstoy"});
        book[2] = new Books("Random Publisher all the same",1862, "The House of the Dead", new String[] {"Fyodor Dostoevsky"});
        book[3] = new Books("Random Publisher all the same",1925, "The Great Gatsby", new String[] {"F Scott Fitzgerald"});
        book[4] = new Books("Random Publisher all the same",1856, "The Snowstorm", new String[] {"Leo Tolstoy"});
        book[5] = new Books("Random Publisher all the same",2016, "Let it Snow", new String[]{"John Green", "Lauren Myracle", "Maureen Johnson", "Dmitriy Ovsyannikov"});
        book[6] = new Books("Random Publisher all the same",2010, "Book of Dares", new String[]{"David Levithan","Rachel Cohn","Emma Giordano"});
        book[7] = new Books("Random Publisher all the same",2015, "Illuminae", new String[]{"Amie Kaufman","Jay Kristoff","Lauren Myracle"});
        return book;
    }
}
