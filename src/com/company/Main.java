package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task1dot1();
    }

    public static void Task1dot1() {

//        1.1 Дополнение к первому заданию - книга дополнительно содержит издательство,
//                может содержать нескольких авторов. Необходимо: найти книги определенного автора,
//        написанные в соавторстве с кем-то (т.е. он не единственный автор книги) и вывести всю инфу по ним;
//        найти книги, где 3 и более авторов и вывести всю инфу по ним.


        System.out.println("Task 1.1");

        // найти книги определенного автора,
        ////        написанные в соавторстве с кем-то (т.е. он не единственный автор книги) и вывести всю инфу по ним;
        Scanner scanner = new Scanner(System.in);
        System.out.println(String.format("Кинги какого автора в сооавторстве с кем то вы хотели бы найти " +
                "(%s >> этот автор должен сработать) ?", "Lauren Myracle"));
        String desiredAuthor = scanner.nextLine();
        BookShelf bookShelf = new BookShelf();
        bookShelf.findBookWithSpecificAuthorAndHaveCoAuthors(desiredAuthor);

        //найти книги, где 3 и более авторов и вывести всю инфу по ним.
        System.out.println("Находим книги с тремя и более авторами и выводим по ним инфу: ");
        bookShelf.findTheBooksWithThreeOrMoreAuthors();
    }
}
